import React, {PureComponent} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import VideoPlayer from './component/videoPlayer';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      videos: [],
      processing: false,
      recording: false,
      index: '',
      videoSource: '',
      showModel: false,
      hidePreViewButton: true,
    };
  }

  //--------------------------->>METHODS<<--------------------------//

  uploadVideo = async () => {
    this.setState({processing: true});
    const type = `video/mp4`;
    const data = new FormData();
    const uri = this.state.vidSource;
    data.append('video', {
      name: 'mobile-video-upload',
      type,
      uri,
    });
    //-----uncomment to impliment API ----------//
    // try {
    //   await fetch(ENDPOINT, {
    //     method: 'post',
    //     body: data,
    //   });
    // } catch (e) {
    //   console.error(e);
    // }
    //Alert.alert('video uploaded Successfully');
    this.setState({processing: false});
  };

  uploadVideoPressed = () => {
    const options = {
      title: 'Video Picker',
      takePhotoButtonTitle: 'Take Video...',
      mediaType: 'video',
      videoQuality: 'medium',
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled video picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        //this.props.parentCallback(response.uri)
        console.log(response);
        this.setState(
          {
            videoSource: response.uri,
            hidePreViewButton: false,
          },
          () => {
            this.uploadVideo();
          },
        );
      }
    });
  };

  playPreview = () => {
    this.setState({showModel: true});
  };

  updateState = () => {
    this.setState({vidSource: url}, () => {
      this.uploadVideo();
    });
  };

  handlePress() {
    this.setState({showModel: false});
  }

  deleteVideoPressed = () => {
    this.setState({videoSource: ''});
  };

  //--------------------------->>RENDER<<--------------------------//

  render() {
    return (
      <View style={styles.container}>
        <View style={{paddingTop: 20}}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.uploadVideoPressed}>
            <Text style={{color: 'white'}}>upload video</Text>
          </TouchableOpacity>
        </View>

        {this.state.videoSource !== '' ? (
          <View style={{alignItems: 'center'}}>
            <View style={{paddingTop: 20}}>
              <TouchableOpacity
                style={styles.button}
                onPress={this.playPreview}>
                <Text style={{color: 'white'}}>Video Preview</Text>
              </TouchableOpacity>
            </View>
            <View style={{paddingTop: 20}}>
              <TouchableOpacity
                style={styles.button}
                onPress={this.deleteVideoPressed}>
                <Text style={{color: 'white'}}>delete video</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <></>
        )}

        {this.state.showModel ? (
          <VideoPlayer
            showModel={this.state.showModel}
            url={this.state.videoSource}
            handlePress={() => this.handlePress()}
          />
        ) : (
          <></>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: 40,
    backgroundColor: '#0A79DF',
    borderWidth: 0.3,
    borderColor: 'gray',
    width: '80%',
    borderRadius: 10,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
