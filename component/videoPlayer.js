import React, {Component} from 'react';
import {StyleSheet, View, Modal, Text, Platform} from 'react-native';
import Video from 'react-native-video';
import MediaControls, {PLAYER_STATES} from 'react-native-media-controls';

export class VideoPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: 'content',
      showChildModel: false,
    };
  }

  componentDidMount() {
    if (this.props && this.props.showModel) {
      let showChildModel = this.props.showModel;
      this.setState({showChildModel: showChildModel});
    }
  }

  //--------------------->>METHODS<<---------------------//

  onSeek = (seek) => {
    //Handler for change in seekbar
    this.videoPlayer.seek(seek);
  };

  onPaused = (playerState) => {
    //Handler for Video Pause
    this.setState({
      paused: !this.state.paused,
      playerState,
    });
  };

  onReplay = () => {
    //Handler for Replay
    this.setState({playerState: PLAYER_STATES.PLAYING});
    this.videoPlayer.seek(0);
  };

  onProgress = (data) => {
    const {isLoading, playerState} = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({currentTime: data.currentTime});
    }
  };

  onLoad = (data) => this.setState({duration: data.duration, isLoading: false});

  onLoadStart = (data) => this.setState({isLoading: true});

  onEnd = () =>
    this.setState({playerState: PLAYER_STATES.ENDED}, () => {
      this.props.handlePress();
      this.setState({showChildModel: false});
    });

  onError = () => alert('Oh! ', error);

  exitFullScreen = () => {
    alert('Exit full screen');
  };

  enterFullScreen = () => {};

  onFullScreen = () => {
    if (this.state.screenType == 'content')
      this.setState({screenType: 'cover'});
    else this.setState({screenType: 'content'});
  };
  renderToolbar = () => (
    <View>
      <Text> toolbar </Text>
    </View>
  );
  onSeeking = (currentTime) => this.setState({currentTime});

  //--------------------------->>RENDER<<--------------------------//

  render() {
    const {url} = this.props;
    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={this.state.showChildModel}
        onRequestClose={() => {
          this.props.handlePress();
          this.setState({showModal: false});
          console.log('close modal');
        }}>
        <View style={styles.container}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Video
              onEnd={this.onEnd}
              onLoad={this.onLoad}
              onLoadStart={this.onLoadStart}
              onProgress={this.onProgress}
              paused={this.state.paused}
              ref={(videoPlayer) => (this.videoPlayer = videoPlayer)}
              resizeMode={
                Platform.OS === 'ios' ? this.state.screenType : 'cover'
              }
              onFullScreen={this.state.isFullScreen}
              source={{
                uri: url,
              }}
              style={styles.mediaPlayer}
              volume={10}
            />
            <MediaControls
              duration={this.state.duration}
              isLoading={this.state.isLoading}
              mainColor="#333"
              onFullScreen={this.onFullScreen}
              onPaused={this.onPaused}
              onReplay={this.onReplay}
              onSeek={this.onSeek}
              onSeeking={this.onSeeking}
              playerState={this.state.playerState}
              progress={this.state.currentTime}
              toolbar={this.renderToolbar()}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
  button: {
    height: 40,
    backgroundColor: '#0A79DF',
    borderWidth: 0.3,
    borderColor: 'gray',
    width: '100%',
    borderRadius: 10,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default VideoPlayer;
